package com.accenture.corpmeal.controller;

import com.accenture.corpmeal.model.User;
import com.accenture.corpmeal.service.CustomUserService;
import com.accenture.corpmeal.model.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    /*
     * @RequestMapping(value="/login", method = RequestMethod.GET) public
     * ModelAndView getLoginForm(
     * 
     * @RequestParam(required = false) String authfailed, String logout, String
     * denied) { String message = ""; if (authfailed != null) { message =
     * "Invalid username of password, try again !"; } else if (logout != null) {
     * message = "Logged Out successfully, login again to continue !"; } else if
     * (denied != null) { message = "Access denied for this user !"; } return
     * new ModelAndView("login", "message", message); }
     */

    @Autowired
    CustomUserService userservice;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView getIndex() {
        return new ModelAndView("index");
    }
    /*
     * @RequestMapping public String geUserPage() { return "/user"; }
     * 
     * @RequestMapping("/admin") public String geAdminPage() { return "/admin";
     * }
     */

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getList() {
        return new ModelAndView("list");
    }

    @RequestMapping("/403page")
    public String ge403denied() {
        return "403page";
    }
    @RequestMapping("/authFailed")
    public String getAuthFailed() {
        return "authFailed";
    }

    @RequestMapping(value = "/AdminUserList", method = RequestMethod.GET)
    public ModelAndView getUserList() {
        ModelAndView model = new ModelAndView("AdminUserList");

        List<UserAdminInfo> ShowList = new ArrayList<UserAdminInfo>();
        List<User> users = new ArrayList<User>();
        users = userservice.listAllUsers();
        User[] UsersTable = new User[users.size()];
        users.toArray(UsersTable);
        for (User U : UsersTable) {
            UserAdminInfo tmp = new UserAdminInfo(U.getLogin(), U.isAutorized());
            tmp.setId( U.getIduser() );
            Set<UsersAuthentication> SetU = U.getUsersAuthentications();
            if (SetU.isEmpty()) {
                tmp.Ban();
            } else {
                for (UsersAuthentication UA : SetU) {
                    String rola = userservice.Role(UA.getIduserAuthentication());
                    if(rola.equals("ROLE_USER"))tmp.makeUser();
                    if(rola.equals("ROLE_ADMIN"))tmp.makeAdmin();
                }

            }

            ShowList.add(tmp);
        }

        for(int i = 1; i < ShowList.size();i++)
        {
            if(ShowList.get(i).getName().equals(ShowList.get(i-1).getName()))ShowList.remove(i);
            
        }
        
        
        model.addObject("users", ShowList);

        return model;

    }
    
    
    @RequestMapping(value = "/AdminUserList/{id}/{Cmd}", method = RequestMethod.GET)
    public ModelAndView getUserList2(@PathVariable("id") Long id,@PathVariable("Cmd") String Comand) {
        ModelAndView model = new ModelAndView("AdminUserList");
       
        String Warn = "User ";
        Warn += userservice.getUserLogin(id);
        Warn +=" was ";
        
        if(Comand.equals("Auth"))
        {
            userservice.activateUser(id);
        Warn += "authorized";
        
        }
        else if(Comand.equals("UnAuth"))
        {
            userservice.deactivateUser(id);
            Warn += "unauthorized";
        }
        else if(Comand.equals("Ban"))
        {
            userservice.ban(id);
            Warn += "baned";
        }
        else if(Comand.equals("unBan"))
        {
           userservice.unBan(id);
          Warn += "unbaned"; 
        }
        else if(Comand.equals("Admin"))
        {
           userservice.makeAdmin(id);
          Warn += "promoted to admin"; 
        }
        else if(Comand.equals("notAdmin"))
        {
           userservice.NotAdmin(id);
          Warn += "unpromoted from admin"; 
        }
        else if(Comand.equals("User"))
        {
           userservice.makeUser(id);
          Warn += "promoted to user"; 
        }
        else if(Comand.equals("notUser"))
        {
           userservice.NotUser(id);
          Warn += "unpromoted from user"; 
        }
        else Warn = "Error";
                
        model.addObject("message",Warn);
       // wy�wietla liste user�w tak jak wyzej  
    List<UserAdminInfo> ShowList = new ArrayList<UserAdminInfo>();
        List<User> users = new ArrayList<User>();
        users = userservice.listAllUsers();
        User[] UsersTable = new User[users.size()];
        users.toArray(UsersTable);
        for (User U : UsersTable) {
            UserAdminInfo tmp = new UserAdminInfo(U.getLogin(), U.isAutorized());
            tmp.setId( U.getIduser() );
            Set<UsersAuthentication> SetU = U.getUsersAuthentications();
            if (SetU.isEmpty()) {
                tmp.Ban();
            } else {
                for (UsersAuthentication UA : SetU) {
                    String rola = userservice.Role(UA.getIduserAuthentication());
                    if(rola.equals("ROLE_USER"))tmp.makeUser();
                    if(rola.equals("ROLE_ADMIN"))tmp.makeAdmin();
                }

            }

            ShowList.add(tmp);
        }

        for(int i = 1; i < ShowList.size();i++)
        {
            if(ShowList.get(i).getName().equals(ShowList.get(i-1).getName()))ShowList.remove(i);
            
        }
        
        
        model.addObject("users", ShowList);    
        
    return model;
    }
    
}
