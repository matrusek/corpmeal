package com.accenture.corpmeal.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.corpmeal.dao.UserDAO;
import com.accenture.corpmeal.service.*;
@Controller
public class HomeController {

	@Autowired
	UserDAO userDAO;
	@Autowired
	CustomUserService service;
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView getHome() {

		return new ModelAndView("index", "list", userDAO.findById(1));
	}
	 @RequestMapping(value = "/register", method = RequestMethod.GET)
		public ModelAndView registrationPage() {

			return new ModelAndView("register");
		}
	 @RequestMapping(value = "/register", method = RequestMethod.POST)
		public ModelAndView register(HttpServletRequest request) {
		 String email = request.getParameter("email");
		 String pass = request.getParameter("pass");
		 try {
			service.createUser(email,pass);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		 return post();
	 }

	@RequestMapping(value = "/post-register", method = RequestMethod.GET)
	public ModelAndView post() {
		return new ModelAndView("post-register");

	}
}
