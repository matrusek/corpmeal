/*
 TYMCZASOWA NIE BRA� NA POWA�NIE
 */
package com.accenture.corpmeal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.corpmeal.dao.UserDAO;
import com.accenture.corpmeal.service.*;

@Controller
public class EverythingController {

    @RequestMapping(value = "/403page", method = RequestMethod.GET)
    public ModelAndView get403() {
        return new ModelAndView("403page");
    }
    @RequestMapping(value = "/AddToMenu", method = RequestMethod.GET)
    public ModelAndView getAddToMenu() {
        return new ModelAndView("AddToMenu");
    }
    @RequestMapping(value = "/profil", method = RequestMethod.GET)
    public ModelAndView getProfil() {
        return new ModelAndView("profil");
    }
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ModelAndView getCart() {
        return new ModelAndView("cart");
    }

}
