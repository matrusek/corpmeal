package com.accenture.corpmeal.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.corpmeal.dao.UserDAO;
import com.accenture.corpmeal.dao.UsersAuthenticationDAO;
import com.accenture.corpmeal.dao.AuthenticationDAO;
import com.accenture.corpmeal.model.PaymentHistory;
import com.accenture.corpmeal.model.User;
import com.accenture.corpmeal.model.UsersAuthentication;
import com.accenture.corpmeal.model.Authentication;

@Service
public class CustomUserService {
	@Autowired
	private UserDAO userdao;
	@Autowired
	private UsersAuthenticationDAO authenticationdao;
	@Autowired
	private AuthenticationDAO authentdictionarydao;

	public void createUser(String name, String password) {
		User user = new User();
		User userBis = new User();
		List<User> users = new ArrayList<User>();
		users = listAllUsers();
		userBis = users.get(users.size() - 1);
		long ID = userBis.getIduser();
		user.setDataofcreation(new Date());
		user.setPassword(password);
		user.setLogin(name);
		user.setIduser(ID + 1);
		BigDecimal bd = BigDecimal.valueOf(0.0);
		user.setSaldo(bd);
		userdao.persist(user);
	}

        public UsersAuthentication GenerateUserAuthentication(User U, Authentication A)
        {
            UsersAuthentication UA = new UsersAuthentication();
            UA.setAuthentication(A);
            UA.setUser(U);
            List<UsersAuthentication> LUA =  authenticationdao.findAll();
            UA.setIduserAuthentication(LUA.get(LUA.size()-1).getIduserAuthentication() + 1  );
            return UA;
        }
        
        
	public void changePermissions(String name, String permission) {
		User user = new User();
		user.setLogin(name);
		user = (User) userdao.findByExample(user).get(0);
		Set authentications;

		authentications = user.getUsersAuthentications();
		if (authentications == null) {
			// TODO
		}
		if (authentications.contains(permission)) {
		} else {
			authentications.clear();
			authentications.add(permission);
		}

		user.setUsersAuthentications(authentications);
		userdao.persist(user);
	}

	/*
	 * public void banUser(String name) { User user = new User();
	 * user.setLogin(name); user = (User)userdao.findByExample(user).get(0); Set
	 * authentications; authentications = user.getUsersAuthentications();
	 * authentications.clear(); user.setUsersAuthentications(authentications);
	 * userdao.persist(user); }
	 */

//        public void banUser(long id) 
//        {
//            User user = userdao.findById(id);
//            Set<UsersAuthentication> userauthentications = user.getUsersAuthentications();
//            for(UsersAuthentication UA:userauthentications)authenticationdao.delete(UA);
//        }
          
        public void makeUser(long id)
        {
            User user = userdao.findById(id);
            UsersAuthentication UA ;
            Authentication A = authentdictionarydao.findById(1);
            
            UA  = GenerateUserAuthentication(user, A);
            authenticationdao.persist(UA);
  
        }
        
        public void NotUser(long id)
        {
           User user = userdao.findById(id); 
            Set<UsersAuthentication> SetAU = user.getUsersAuthentications();
            Authentication A = authentdictionarydao.findById(1);
            for(UsersAuthentication UA : SetAU)
            {
                 String rola = Role(UA.getIduserAuthentication());
                if(rola.equals("ROLE_USER"))authenticationdao.delete(UA);
            }
        }
        
         public void NotAdmin(long id)
        {
           User user = userdao.findById(id); 
            Set<UsersAuthentication> SetAU = user.getUsersAuthentications();
            Authentication A = authentdictionarydao.findById(1);
            for(UsersAuthentication UA : SetAU)
            {
                 String rola = Role(UA.getIduserAuthentication());
                if(rola.equals("ROLE_ADMIN"))authenticationdao.delete(UA);
            }
        }
       
         public void ban(long id)
         {
           User user = userdao.findById(id); 
            Set<UsersAuthentication> SetAU = user.getUsersAuthentications();
            for(UsersAuthentication UA : SetAU)
            {
                authenticationdao.delete(UA);
            }
        }
                 
        
        public void unBan(long id)
        { 
            makeUser(id);
        }
        
         public void makeAdmin(long id)
        {
            User user = userdao.findById(id);
            UsersAuthentication UA ;
            Authentication A = authentdictionarydao.findById(2);
            
            UA  = GenerateUserAuthentication(user, A);
            authenticationdao.persist(UA);
  
        }
        
	public void activateUser(String name) {

		User user = new User();
		user.setLogin(name);
		user = (User) userdao.findByExample(user).get(0);
		user.setAutorized(true);

		userdao.persist(user);
	}

        public void activateUser(long id) {

		
		User user = userdao.findById(id);
		user.setAutorized(true);
		userdao.merge(user);
	}
        
        public void deactivateUser(long id) {

		
		User user = userdao.findById(id);
		user.setAutorized(false);
		userdao.merge(user);
	}
        
        
        
	public void deleteUser(String name) {

		User user = new User();
		user.setLogin(name);
		user = (User) userdao.findByExample(user).get(0);
		userdao.delete(user);
	}
        
        
	public List<User> listAllUsers() {
		List<User> users = new ArrayList<User>();
		users = userdao.findAll();
		return users;

	}

	public List<UsersAuthentication> listAllUsersAuthetication() {
		List<UsersAuthentication> usersA = new ArrayList<UsersAuthentication>();
		usersA = authenticationdao.findAll();
		return usersA;

	}

	public Set<PaymentHistory> getHistory(String name) {
		Set<PaymentHistory> paymentHistory = new HashSet<PaymentHistory>();
		return paymentHistory;
	}

	public List<UsersAuthentication> ShowPriviliges(User user) {
		UsersAuthentication userauth = new UsersAuthentication();
		userauth.setUser(user);
		List<UsersAuthentication> ListaAutoryzacji = authenticationdao
				.findByExample(userauth);
		return ListaAutoryzacji;

	}

	public int isUser(User user) {
		UsersAuthentication userauth = new UsersAuthentication();
		userauth.setUser(user);
		List<UsersAuthentication> ListaAutoryzacji = authenticationdao
				.findByExample(userauth);
		UsersAuthentication[] TabAutoryzacji = new UsersAuthentication[ListaAutoryzacji
				.size()];
		ListaAutoryzacji.toArray(TabAutoryzacji);
		int i = 0;
		for (UsersAuthentication x : TabAutoryzacji) {
			Authentication auth = x.getAuthentication();
			auth = authentdictionarydao.findById(auth.getIdAuthentication());
			i += (int) auth.getIdAuthentication();
			// if(auth.equals("ROLE_USER") )return true;
		}
		return i;

		// Authentication auth = new Authentication();
		// auth.setName("ROLE_USER");
		// auth = authentdictionarydao.findByExample(auth).get(0);
		// UsersAuthentication userauth = new UsersAuthentication();
		// userauth.setUser(user);
		// userauth.setAuthentication(auth);
		// if(authenticationdao.findByExample(userauth).isEmpty() )return false;
		// else return true;

	}

	public String Role(long idUserAuthentication) {
		UsersAuthentication UA = authenticationdao
				.findById(idUserAuthentication);
		Authentication A = UA.getAuthentication();
		A = authentdictionarydao.findById(A.getIdAuthentication());
		return A.getName();
	}

        public String getUserLogin(long id)
        {
            return userdao.findById(id).getLogin();
        }
        
}
