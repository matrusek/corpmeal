package com.accenture.corpmeal.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.corpmeal.dao.MenuDAO;
import com.accenture.corpmeal.dao.OrderDAO;
import com.accenture.corpmeal.dao.RestaurantDAO;
import com.accenture.corpmeal.model.Menu;
import com.accenture.corpmeal.model.Order;
import com.accenture.corpmeal.model.Restaurant;
import com.accenture.corpmeal.model.User;
@Service
public class OrderDetailService {
	@Autowired
	RestaurantDAO restaurantDAO;
	@Autowired
	MenuDAO menuDAO;
	@Autowired
	OrderDAO orderDAO;

	public void addRestaurant(String name, String adres, String URL,
			Long telephone, Integer MaxDeliveryCost) {
		Restaurant restaurant = new Restaurant();
		restaurant.setName(name);
		if (adres != null)
			restaurant.setAdress(adres);
		if (URL != null)
			restaurant.setUrl(URL);
		if (telephone != null)
			restaurant.setTelephone(telephone);
		if (MaxDeliveryCost != null)
			restaurant.setMaxDeliveryCost(MaxDeliveryCost);
		restaurantDAO.persist(restaurant);
	}

	public void addMenu(long idRestartant, String name, String decription,
			Float price) {
		Restaurant restaurant = new Restaurant();
		restaurant.setIdrestaurant(idRestartant);
		restaurant = restaurantDAO.findByExample(restaurant).get(0);
		com.accenture.corpmeal.model.Menu menu = new Menu(restaurant, name,
				decription, price);
		menuDAO.persist(menu);
	}

	public void addMenu(String RestarantName, String name, String decription,
			Float price) {

		Restaurant restaurant = getRestaurant(RestarantName);
		com.accenture.corpmeal.model.Menu menu = new Menu(restaurant, name,
				decription, price);
		menuDAO.persist(menu);
	}

	public List<Restaurant> listRestaurants() {
		return restaurantDAO.findByExample(new Restaurant());
	}

	public List<Menu> listMenus(Restaurant restaurant) {
		return menuDAO.findByExample(new Menu());
	}

	public Restaurant getRestaurant(String name) {
		Restaurant restaurant = new Restaurant();
		restaurant.setName(name);
		restaurant = restaurantDAO.findByExample(restaurant).get(0);
		return restaurant;
	}

	public void deleteMenu(long id) {
		menuDAO.delete(menuDAO.findById(id));
	}

	public void deleteMenu(String name) {
		Menu menu = new Menu();
		menu.setName(name);
		menuDAO.delete(menuDAO.findByExample(menu).get(0));
	}

	public void deleteRestaurant(String name)
	{
		Restaurant restaurant = getRestaurant(name);
		Menu menu = new Menu();
		menu.setRestaurant(restaurant);
                List <Menu> ListMenu = menuDAO.findByExample(menu);
                Menu[] TableMenu = new Menu[ListMenu.size()];
                 ListMenu.toArray(TableMenu);
                
		for (Menu menuBis : TableMenu) {
			menuDAO.delete(menuBis);
		}
		restaurantDAO.delete(restaurant);
	}

	public void addOrder(User user, Menu menu) {
		Order order = new Order(menu, user, new Date(), false);
		orderDAO.persist(order);
	}

	public void fulfiledOrder(Order order) {
		// TODO:Sparwdiz� czy trzyma si� kupy
		order =(Order)orderDAO.findByExample(order).get(0);
		order.setIsFulified(true);
		orderDAO.merge(order);
	}

}
