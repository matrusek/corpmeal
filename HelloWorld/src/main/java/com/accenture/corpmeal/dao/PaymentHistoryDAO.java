package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.PaymentHistory;

public interface PaymentHistoryDAO {
	public void persist(PaymentHistory transientInstance);

	public void delete(PaymentHistory persistentInstance);

	public PaymentHistory merge(PaymentHistory detachedInstance);

	public PaymentHistory findById(long id);

	public List findByExample(PaymentHistory instance);
}
