package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.User;

public interface UserDAO {
	public void persist(User transientInstance);

	public void delete(User persistentInstance);

	public User merge(User detachedInstance);

	public User findById(long id);

	public List findByExample(User Instance);
        
        public  List findAll();
}
