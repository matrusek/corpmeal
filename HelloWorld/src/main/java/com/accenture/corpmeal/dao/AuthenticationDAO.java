package com.accenture.corpmeal.dao;

import java.util.List;
import com.accenture.corpmeal.model.Authentication;

public interface AuthenticationDAO {
	public void persist(Authentication transientInstance);

	public void delete(Authentication persistentInstance);

	public Authentication merge(Authentication detachedInstance);

	public Authentication findById(long id);

	public List<Authentication> findByExample(Authentication instance);
}
