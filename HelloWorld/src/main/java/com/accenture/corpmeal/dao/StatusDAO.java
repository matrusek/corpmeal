package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.Status;

public interface StatusDAO {
	public void persist(Status transientInstance);

	public void delete(Status persistentInstance);

	public Status merge(Status detachedInstance);

	public Status findById(long id);

	public List findByExample(Status instance);
}
