package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.Order;

public interface OrderDAO {
	public void persist(Order transientInstance);

	public void delete(Order persistentInstance);

	public Order merge(Order detachedInstance);

	public Order findById(long id);

	public List findByExample(Order instance);
}
