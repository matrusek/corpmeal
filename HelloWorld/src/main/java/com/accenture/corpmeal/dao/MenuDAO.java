package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.Menu;

public interface MenuDAO {
	public void persist(Menu transientInstance);

	public void delete(Menu persistentInstance);

	public Menu merge(Menu detachedInstance);

	public Menu findById(long id);

	public List<Menu> findByExample(Menu instance);
}
