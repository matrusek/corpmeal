package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.UsersAuthentication;

public interface UsersAuthenticationDAO {
	public void persist(UsersAuthentication transientInstance);

	public void delete(UsersAuthentication persistentInstance);

	public UsersAuthentication merge(UsersAuthentication detachedInstance);

	public UsersAuthentication findById(long id);

	public List <UsersAuthentication> findByExample(UsersAuthentication instance);
        
        public List <UsersAuthentication> findAll();
}
