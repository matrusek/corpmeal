package com.accenture.corpmeal.dao;

import java.util.List;

import com.accenture.corpmeal.model.Restaurant;

public interface RestaurantDAO {
	public void persist(Restaurant transientInstance);

	public void delete(Restaurant persistentInstance);

	public Restaurant merge(Restaurant detachedInstance);

	public Restaurant findById(long id);

	public List<Restaurant> findByExample(Restaurant instance);
}
