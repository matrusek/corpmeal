package com.accenture.corpmeal.dao;

// Generated 2014-12-09 22:34:34 by Hibernate Tools 3.4.0.CR1
import java.util.List;

import javax.annotation.Resource;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.corpmeal.model.UsersAuthentication;

/**
 * Home object for domain model class UsersAuthentication.
 *
 * @see .UsersAuthentication
 * @author Hibernate Tools
 */
@Repository
@Transactional
public class UsersAuthenticationDAOImpl implements UsersAuthenticationDAO {

    private static final Log log = LogFactory.getLog(UsersAuthenticationDAOImpl.class);
    @Resource
    private SessionFactory sessionFactory;

    protected SessionFactory getSessionFactory() {
        try {
            return (SessionFactory) new InitialContext()
                    .lookup("SessionFactory");
        } catch (Exception e) {
            log.error("Could not locate SessionFactory in JNDI", e);
            throw new IllegalStateException(
                    "Could not locate SessionFactory in JNDI");
        }
    }

    public void persist(UsersAuthentication transientInstance) {
        log.debug("persisting UsersAuthentication instance");
        try {
            sessionFactory.getCurrentSession().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void delete(UsersAuthentication persistentInstance) {
        log.debug("deleting UsersAuthentication instance");
        try {
            sessionFactory.getCurrentSession().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public UsersAuthentication merge(UsersAuthentication detachedInstance) {
        log.debug("merging UsersAuthentication instance");
        try {
            UsersAuthentication result = (UsersAuthentication) sessionFactory
                    .getCurrentSession().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public UsersAuthentication findById(long id) {
        log.debug("getting UsersAuthentication instance with id: " + id);
        try {
            UsersAuthentication instance = (UsersAuthentication) sessionFactory
                    .getCurrentSession().get(UsersAuthentication.class, id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List <UsersAuthentication> findByExample(UsersAuthentication instance) {
        log.debug("finding UsersAuthentication instance by example");
        try {
            List results = sessionFactory.getCurrentSession()
                    .createCriteria(UsersAuthentication.class)
                    .add(Example.create(instance)).list();
            log.debug("find by example successful, result size: "
                    + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }
    
    public List <UsersAuthentication> findAll()
    {
        log.debug("finding UserAuthentication instance by example");
        try {
            List results = sessionFactory.getCurrentSession().createCriteria(UsersAuthentication.class).list();
            log.debug("find alle successful, result size: "
                    + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find all failed", re);
            throw re;
        }
        
        
    }
}
