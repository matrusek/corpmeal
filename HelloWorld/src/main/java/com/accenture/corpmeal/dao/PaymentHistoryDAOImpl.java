package com.accenture.corpmeal.dao;

// Generated 2014-12-09 22:34:34 by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.annotation.Resource;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.stereotype.Repository;

import com.accenture.corpmeal.model.PaymentHistory;

/**
 * Home object for domain model class PaymentHistory.
 * 
 * @see .PaymentHistory
 * @author Hibernate Tools
 */
@Repository
public class PaymentHistoryDAOImpl implements PaymentHistoryDAO{

	private static final Log log = LogFactory.getLog(PaymentHistoryDAOImpl.class);

	@Resource
	private SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(PaymentHistory transientInstance) {
		log.debug("persisting PaymentHistory instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void delete(PaymentHistory persistentInstance) {
		log.debug("deleting PaymentHistory instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public PaymentHistory merge(PaymentHistory detachedInstance) {
		log.debug("merging PaymentHistory instance");
		try {
			PaymentHistory result = (PaymentHistory) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public PaymentHistory findById(long id) {
		log.debug("getting PaymentHistory instance with id: " + id);
		try {
			PaymentHistory instance = (PaymentHistory) sessionFactory
					.getCurrentSession().get("PaymentHistory", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(PaymentHistory instance) {
		log.debug("finding PaymentHistory instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("PaymentHistory")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
