/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.accenture.corpmeal.model;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author Patryk
 */
public class UserAdminInfo
{
    private String name;
    private long id;
    private boolean authorized;
    private boolean isUser;
    private boolean isAdmin;
    private boolean isBanned;
    
   public UserAdminInfo()
   {
       
   }
    
   
    
    public UserAdminInfo(String name, boolean authorized)
    {
        
       this.name = name;
        this.authorized =  authorized;
        this.isUser = false;
        this.isAdmin = false;
        this.isBanned = false;

    }
    
    public void Ban()
    {
        this.setIsBanned(true);
    }
    public void makeAdmin()
    {
        this.setIsAdmin(true);
    }
    public void makeUser()
    {
        this.setIsUser(true);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the authorized
     */
    public boolean isAuthorized() {
        return authorized;
    }

    /**
     * @param authorized the authorized to set
     */
    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    /**
     * @return the isUser
     */
    public boolean isIsUser() {
        return isUser;
    }

    /**
     * @param isUser the isUser to set
     */
    public void setIsUser(boolean isUser) {
        this.isUser = isUser;
    }

    /**
     * @return the isAdmin
     */
    public boolean isIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin the isAdmin to set
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return the isBanned
     */
    public boolean isIsBanned() {
        return isBanned;
    }

    /**
     * @param isBanned the isBanned to set
     */
    public void setIsBanned(boolean isBanned) {
        this.isBanned = isBanned;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    
}

