package com.accenture.corpmeal.model;
// Generated 2015-01-23 20:28:59 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Authentication generated by hbm2java
 */
@Entity
@Table(name="authentication"
    ,catalog="corpmeal"
)
public class Authentication  implements java.io.Serializable {


     private long idAuthentication;
     private long version;
     private String name;
     private Set<UsersAuthentication> usersAuthentications = new HashSet(0);

    public Authentication() {
    }

	
    public Authentication(long idAuthentication, String name) {
        this.idAuthentication = idAuthentication;
        this.name = name;
    }
    public Authentication(long idAuthentication, String name, Set usersAuthentications) {
       this.idAuthentication = idAuthentication;
       this.name = name;
       this.usersAuthentications = usersAuthentications;
    }
   
     @Id 

    
    @Column(name="idAuthentication", unique=true, nullable=false)
    public long getIdAuthentication() {
        return this.idAuthentication;
    }
    
    public void setIdAuthentication(long idAuthentication) {
        this.idAuthentication = idAuthentication;
    }

    @Version
    @Column(name="version", nullable=false)
    public long getVersion() {
        return this.version;
    }
    
    public void setVersion(long version) {
        this.version = version;
    }

    
    @Column(name="name", nullable=false, length=45)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="authentication")
    public Set<UsersAuthentication> getUsersAuthentications() {
        return this.usersAuthentications;
    }
    
    public void setUsersAuthentications(Set<UsersAuthentication> usersAuthentications) {
        this.usersAuthentications = usersAuthentications;
    }




}



