<!%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <head>
    <meta http-equiv="Content-type" content="text/html; charset=ISO-8859-2" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Corpmeal MenuAdd</title>

     <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
	  <link rel="stylesheet" type="text/css" href="css/custom.css"/>	

    <!-- Custom styles for this template -->
     <link href="dashboard.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="list.jsp">Corpmeal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
		   <li><a style="color:red"><b>Saldo: 50.00 zł</b></a></li>
		   <li><a class="minicart" href="cart.jsp"></a></li>
		  <li><a href="profil.html">Profil</a></li>
            <li><a href="menu.html">Menu</a></li>
            <li><a href="index.html">Log out</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="column">
        
    <div class="container">
          <h2 class="sub-header">Menu</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Dodawanie nowej pozycji do listy dań  </th>


				  
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Resturacja</td>
				</tr>
				<tr>
                  <td><input type = "text"></td>
				</tr>
				<tr>
                  <td>Nazwa dania</td>
				</tr>
				<tr>
                  <td><input type = "text" value=""></td>
				</tr>
				<tr>
                  <td>Opis</td>
				</tr>
				<tr>
                  <td><TEXTAREA Name="content" ROWS=3 COLS=20></TEXTAREA></td>
				</tr>
				<tr>
                  <td>Cena</td>
				</tr>
				<tr>
                  <td><input type = "text" value="0.00">zł</td>
				</tr>
				<tr>
                  <td>Cena dostawy</td>
				</tr>
				<tr>
                  <td><input type = "text" value="0.00">zł</td>
				</tr>
				<tr>
                  <td><input type = "submit" value="Dodaj"></td>
				</tr>
              </tbody>
            </table>
			</div>
          </div>
      </div>
    </div>
 
	 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value="/js/bootstrap.min.js"/>" >
    </script>
    <script src="<c:url value="/js/ie10-viewport-bug-workaround.js"/>" >
    </script>
  </body>
</html>
