<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Dashboard Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/dashboard.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<%=request.getContextPath()%>/js/ie-emulation-modes-warning.js"></script>


    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <a class="navbar-brand" href="<c:url value="j_spring_security_logout" />">Logout</a>
                    
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Settings</a></li>
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                    <form class="navbar-form navbar-right">
                        <input type="text" class="form-control" placeholder="Search...">
                    </form>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="column">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Menu</a></li>
                        <li><a href="#">Restauracje</a></li>
                        <li><a href="#">Zamowienia</a></li>
                    </ul>
                </div>

                
                
                
                <h2 class="sub-header">abcd</h2>
                <div class="table-responsive">
                    <c:if test="${not empty message}">
                    <h2 class="sub-header">${message}</h2>
                    </c:if>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Login</th>
                                <th>Autoryzcja</th>
                                <th>User</th>
                                <th>Admin</th>
                                <th>Zablokowany</th>
                            </tr>
                        </thead>
                        <tbody>



                            <c:if test="${not empty users}">

                                <c:forEach var="listValue" items="${users}">
                                    <tr>
                                        <td>
                                            ${listValue.name}</td>
                                        <td>
                                            <c:choose>
                                            <c:when test="${listValue.authorized}"  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/UnAuth" /> <img src="<%=request.getContextPath()%>/css/yes.jpg"  alt="Tak" width="20" height="20"/> </a>
                                            </c:when>
                                            <c:otherwise  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/Auth" /><img src="<%=request.getContextPath()%>/css/no.jpg" alt="Nie" width="20" height="20"/></a>
                                            </c:otherwise>
                                            </c:choose ></td>
                                        <td>
                                        <c:choose>
                                            <c:when test="${listValue.isUser}"  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/notUser"/><img src="<%=request.getContextPath()%>/css/yes.jpg"  alt="Tak" width="20" height="20"/> </a>
                                            </c:when>
                                            <c:otherwise  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/User" /><img src="<%=request.getContextPath()%>/css/no.jpg" alt="Nie" width="20" height="20"/> </a>
                                            </c:otherwise>
                                            </c:choose ></td>
                                        <td>
                                            <c:choose>
                                            <c:when test="${listValue.isAdmin}"  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/notAdmin" /><img src="<%=request.getContextPath()%>/css/yes.jpg"  alt="Tak" width="20" height="20"/> </a>
                                            </c:when>
                                            <c:otherwise  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/Admin"/><img src="<%=request.getContextPath()%>/css/no.jpg" alt="Nie" width="20" height="20"/> </a>
                                            </c:otherwise>
                                            </c:choose ></td>
                                        </td>
                                        <td>
                                         <c:choose>
                                            <c:when test="${listValue.isBanned}"  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/unBan" /><img src="<%=request.getContextPath()%>/css/yes.jpg"  alt="Tak" width="20" height="20"/> </a>
                                            </c:when>
                                            <c:otherwise  >
                                                <a  href="<%=request.getContextPath()%>/AdminUserList/${listValue.id}/Ban" /><img src="<%=request.getContextPath()%>/css/no.jpg" alt="Nie" width="20" height="20"/> </a>
                                            </c:otherwise>
                                            </c:choose ></td>
                                        </td>

                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/docs.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<%=request.getContextPath()%>/js/ie10-viewport-bug-workaround.js"></script>
    </body></html>