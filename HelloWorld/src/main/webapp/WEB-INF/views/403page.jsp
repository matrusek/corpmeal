<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>403 page</title>

    <!-- Bootstrap core CSS -->
    <link type=text/css href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link type=text/css href="<c:url value="/jumbotron.css"/> " rel="stylesheet">

  </head>
     <!-- END HEAD -->
     <!-- BEGIN BODY -->
<body  >
      <!-- PAGE CONTENT --> 
<div class="container">
	<div class = "row"><br/></br></div>
		<div class = "row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<div class="alert alert-danger" role="alert">BŁĄD 403</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br /><br />
			<div class="col-lg-8 col-lg-offset-2 text-center">
			<p class="text-muted">Niestety nie masz uprawnie� aby przegl�da� t�� stron�</p>
			<div class="clearfix"></div>
			<br /><br />
			<div class="clearfix"></div>
			<br />
			<div class="col-lg-6  col-lg-offset-3">
			<div class="btn-group btn-group-justified">
			<a href="list.html" class="btn btn-primary">Powr�t do poprzedniej strony</a>	   
		</div>	
	</div>
</div>


</div>
      <!-- END PAGE CONTENT --> 

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value="/js/bootstrap.min.js"/>" >
    </script>
    <script src="<c:url value="/js/ie10-viewport-bug-workaround.js"/>" >
    </script>
</body>
     <!-- END BODY -->
</html>
