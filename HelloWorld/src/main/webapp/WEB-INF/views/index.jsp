<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Corpmeal logging page</title>

    <!-- Bootstrap core CSS -->
    <link type=text/css href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link type=text/css href="<c:url value="/jumbotron.css"/> " rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Corpmeal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" method="post" action="<c:url value='j_spring_security_check'/>">
            <div class="form-group">
              <input type="text" placeholder="login" name ="username" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" name="password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Corpmeal</h1>
        <p>nie masz konta ?</p>
        <p><a class="btn btn-primary btn-lg" type="submit" href="<%=request.getContextPath()%>/register" role="button">Zarejestruj&raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Food Market</h2>
          <p>W ostatnim tygodniu udalo sie nazwiazac z nowa restauracja! Food market to restauracja, w ktorej kazdy znajdzie cos dla siebie...</p>
          <p><a class="btn btn-default" href="#" role="button">Menu &raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Maly Oxford</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script 
    src="<c:url value="/js/bootstrap.min.js"/>" >
    </script>
    <script 
    src="<c:url value="/js/ie10-viewport-bug-workaround.js"/>" >
    </script>
   
  </body>
</html>