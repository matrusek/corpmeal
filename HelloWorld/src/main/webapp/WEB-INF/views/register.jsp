<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Rejestracja</title>

<!-- Bootstrap core CSS -->
<link href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<%=request.getContextPath()%>/dashboard.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>css/bootstrap.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>css/font-awesome.min.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>css/style.css" rel="stylesheet">

<script
	src="<%=request.getContextPath()%>/js/ie-emulation-modes-warning.js"></script>


</head>

<body>
	<div class="container">
		<div class="row text-center pad-top ">
			<div class="col-md-12">
				<h2>Formularz rejestracyjny Corpmeal</h2>
			</div>
		</div>
		<div class="row  pad-top">

			<div
				class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong> Register</strong>
					</div>
					<div class="panel-body">
						<form role="form" action="register" method="post">

							 <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type="text" class="form-control" placeholder="Your Email" name="email"/>
                                        </div>
							<div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" class="form-control" placeholder="Enter Password" name="pass" id="password"/>
                                        </div>
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" class="form-control" placeholder="Retype Password" id="confirm"/>
                                       </div><div><span id='message'></span></div>
							<button type="submit" class="btn btn-success ">Zarejestruj</button>
                                    <hr />
                                    Masz konto ?  <a href="<%=request.getContextPath()%>/index" >Zaloguj </a>

						</form>
					</div>

				</div>
			</div>


		</div>
	</div>



	<!-- Bootstrap core JavaScript
        ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>
$('#confirm').on('keyup', function () {
    if ($(this).val() == $('#password').val()) {
        $('#message').html('matching').css('color', 'green');
    } else $('#message').html('not matching').css('color', 'red');
});
</script>
	<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/docs.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="<%=request.getContextPath()%>/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>