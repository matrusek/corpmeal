<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Post Register</title>

    <!-- Bootstrap core CSS -->
    <link type=text/css href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link type=text/css href="<c:url value="/jumbotron.css"/> " rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1 style="color:red">Login Failed</h1>
        <p>Incorrect login or/and password</p>
        <p><a class="btn btn-primary btn-lg" href="<%=request.getContextPath()%>/index" role="button">login page &raquo;</a></p>
      </div>
    </div>

   
      <hr>

     
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script 
    src="<c:url value="/js/bootstrap.min.js"/>" >
    </script>
    <script 
    src="<c:url value="/js/ie10-viewport-bug-workaround.js"/>" >
    </script>
   
  </body>
</html>