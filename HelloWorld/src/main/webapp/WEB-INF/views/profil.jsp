<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Profile</title>

    <!-- Bootstrap core CSS -->
    <link type=text/css href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link type=text/css href="<c:url value="/jumbotron.css"/> " rel="stylesheet">

  </head>
  <body>
     <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="list.html">Corpmeal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
		   <li><a style="color:red"><b>Saldo: 50.00 zł</b></a></li>
		   <li><a class="minicart" href="cart.html"></a></li>
		  <li><a href="profil.html">Profil</a></li>
            <li><a href="menu.html">Menu</a></li>
            <li><a href="index.html">Log out</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
            
          </ul>
        </div>
      </div>
	</div>
          <div class="row placeholders">
            <div class="col-xs-6 col-sm-4 placeholder">
              <img src="http://beelinetour.com/wp-content/uploads/2014/05/avatar.jpg" class="img-responsive" alt="Generic placeholder thumbnail">
            </div>
            <div class="col-xs-6 col-sm-4 placeholder">
				<h2 class="sub-header">Dane użytkownika</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>Nazwa</b></td>
                  <td>kabans.kabans</td>
                </tr>
                <tr>
                  <td><b>Mail</b></td>
                  <td>kabanos@MalyOxford.pl</td>
                </tr>
                <tr>
                  <td><b>Data rejestracji</b></td>
                  <td>2015-02-05</td>
                </tr>
               
              </tbody>
            </table>
			<p><a class="btn btn-default" href="" role="button">Zmień hasło</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-default" href="" role="button">Zmień adres mail</a></p>
			</div>
			<h2 class="sub-header">Historia Zamówień</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>lp</th>
                  <th>Restauracja</th>
                  <th>Danie</th>
                  <th>Koszt</th>
                  <th>Data realizacji</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Food Market</td>
                  <td>Schab pieczony</td>
                  <td>12 PLN</td>
                  <td>12-01-2015</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Finestra</td>
                  <td>Pizza Mafiozo</td>
                  <td>21 PLN</td>
                  <td>11-01-2015</td>
                </tr>
				 <tr>
                  <td>3</td>
                  <td>Finestra</td>
                  <td>Pizza Mafiozo</td>
                  <td>21 PLN</td>
                  <td>10-01-2015</td>
                </tr>
				 <tr>
                  <td>4</td>
                  <td>Finestra</td>
                  <td>Pizza Mafiozo</td>
                  <td>21 PLN</td>
                  <td>9-01-2015</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Food Market</td>
                  <td>Schab pieczony</td>
                  <td>12 PLN</td>
                  <td>8-01-2015</td>
                </tr>
               
              </tbody>
            </table>
			</div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value="/js/bootstrap.min.js"/>" >
    </script>
    <script src="<c:url value="/js/ie10-viewport-bug-workaround.js"/>" >
    </script>
  </body>
</html>